# earthnew.media

**This is a work in progress**. See the project roadmap below or latest [BDD Test Report](https://earthnewmedia.gitlab.io/enm-workspace) for latest udates.

**Phase**: `p1-elaboration`

**Release**: `v0.1a`

`earthnew.media` is provided by [Washington Web Apps](https://www.waweb.io/) under the MIT license. Registered 
trademarks of Washington Web Apps include _earthnew.media_, _earthnewmedia_, and _EnmCoin_, and may not be used
in any reproduction of this work.

## Getting Started

The `earthnew.media` projects are intended to be built with minmal host configuration. Suported build hosts are Windows, Linux, and Mac. The only system requirement for build hosts is that Java JDK 1.8 or newer is installed installed.

Run the following commands to do full build including integration tests and reporting.

```
git clone --recursive --recursive-submodules https://gitlab.com/earthnewmedia/enm-workspace.git
cd enm-workspace
./gradlew build
# or ./gradlew.bat on windows
```

## Components

The following projects provide the implementation and tooling for the `earthnewmedia` group

- [enm-workspace](https://gitlab.com/earthnewmedia/enm-workspace): Top Level Workspace (this project)
- [enm-gradle](https://gitlab.com/earthnewmedia/enm-gradle): Gradle Plugin Projects
- [enm-client](https://gitlab.com/earthnewmedia/enm-client): Frontend Client Projects
- [enm-server](https://gitlab.com/earthnewmedia/enm-server): Backend Server Projects
- [enm-modeling](https://gitlab.com/earthnewmedia/enm-modeling): Domain Modeling Projects

## Project Documentation
- [Project Inception](docs/project_inception.md)
- [Project Roadmap](docs/project_roadmap.md)
- [Systems Design](docs/systems_design.md)
