# earthnew.media Project Inception

## Goal

The primary goal of `earthnew.media` is to build a federated social networking platform that can gain mass-market adoption and enable a new era of social networking without the precondition of add/tracking based revenue models.


## Problem Statement

There have been a few attempts at federated social networking before, but to this date, they have failed to gain significant traction for one or more of the following reasons:

1. Complicated webs of trust make user/content discovery and synchronization difficult, or in contrast, a a lack of a proper trust model leading to rampant spam and astroturfing

2. The network is slow and/or unstable due to a lack of incentives for peer hosting arrangements

3. Reliable multi-region hosting of large static assets such as image or video can be expensive, and someone will need to foot the bill at the end of the day

4. The lack of paid incentives for content producers creates a healthy imbalance of producers to consumers on the network


## Solution Proposal

To solve many of these issues at the core `earthnew.media` intends to provide an Initial Coin Offering (ICO) named _EnmCoin_. EnmCoin is a crypto-currency where transactions are linked to events on the network. This could include but is not limited to activities such as commenting, voting (likes), creating a new post, etc. In addition to supporting micro-payments in the spirit of Patreon and similar, some events may induce penalties paid out from one member account to another. These penalties may be utilized by content producers and hosting peers to enable an add-free revenue model that incentivizes sharing and openness between producers and consumers.