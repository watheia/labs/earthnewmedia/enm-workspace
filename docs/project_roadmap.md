# earthnew.media Project Roadmap

Each phase will be broken into at least 2 rounds of a [Unified Software Development Process](https://en.wikipedia.org/wiki/Unified_Process). A stable version will be prepared for release at the end of each phase.

### Phase I

Basic UX and API functionality for a simple media-focused social networking site.

- `v0.1a` Alpha release with focus on getting "pen to paper"
- `v0.1b` Beta release with focus on improving UX, stability, and performance
- `v0.1.0` MVP reference implementation establishing basic contracts between client and server APIs

### Phase II

Design and implement the network federation protocols, such as account synchronization, content hosting, and decentralized trust

- `v0.2a` _TBD_
- `v0.2b`_TBD_
- `v0.2.0` _TBD_

### Phase III

Establish EnmCoin ICO, and implement related functionality in application

- `v0.3a` _TBD_
- `v0.3b` _TBD_
- `v1.0.0` _TBD_
