# earthnew.media Systems Design & Architecture

## Design Goals

To achieve the project goals, the systems architecture must allow for a high degree of customizability while maintaining the integrity and stability of the core platform. To accomplish this goal `earthnew.media` borrows heavily from [Enterprise Integration Patterns](https://www.enterpriseintegrationpatterns.com/patterns/messaging/toc.html) from the excellent book by Gregor Hohpe and Bobby Woolf.

## Terminoligy

The following terms may be used to describe concepts within the systems architecture.

### Messaging Systems

* `Point to Point Channel` - How can the caller be sure that exactly one receiver will receive the document or perform the call?

* `Message Channel` - How does one application communicate with another using messaging?

* `Message` - How can two applications connected by a message channel exchange a piece of information?

* `Pipes and Filters` - How can we perform complex processing on a message while maintaining independence and flexibility?

* `Message Router` - How can you decouple individual processing steps so that messages can be passed to different filters depending on a set of conditions?

* `Message Translator` - How can systems using different data formats communicate with each other using messaging?

* `Message Endpoint` - How does an application connect to a messaging channel to send and receive messages?

### Messaging Channels

* `Publish Subscribe Channel` - How can the sender broadcast an event to all interested receivers?

* `Dead Letter Channel` - What will the messaging system do with a message it cannot deliver?

* `Guaranteed Delivery` - How can the sender make sure that a message will be delivered, even if the messaging system fails?

* `Channel Adapter` - How can you connect an application to the messaging system so that it can send and receive messages?

* `Messaging Bridge` - How can multiple messaging systems be connected so that messages available on one are also available on the others??

* `Message Bus` - What is an architecture that enables separate applications to work together, but in a de-coupled fashion such that applications can be easily added or removed without affecting the others?

* `Change Data Capture` - Data synchronization by capturing changes made to a database, and apply those changes to another system.

### Message Construction

* `Event Message` - How can messaging be used to transmit events from one application to another?

* `Request Reply` - When an application sends a message, how can it get a response from the receiver?

* `Return Address` - How does a replier know where to send the reply?

* `Correlation Identifier` - How does a requestor that has received a reply know which request this is the reply for?

* `Message Expiration` - How can a sender indicate when a message should be considered stale and thus shouldn’t be processed?

### Message Routing

* `Content Based Router` - How do we handle a situation where the implementation of a single logical function (e.g., inventory check) is spread across multiple physical systems?

* `Message Filter` - How can a component avoid receiving uninteresting messages?

* `Dynamic Router` - How can you avoid the dependency of the router on all possible destinations while maintaining its efficiency?

* `Recipient List` - How do we route a message to a list of (static or dynamically) specified recipients?

* `Splitter` - How can we process a message if it contains multiple elements, each of which may have to be processed in a different way?

* `Aggregator` - How do we combine the results of individual, but related messages so that they can be processed as a whole?

* `Resequencer` - How can we get a stream of related but out-of-sequence messages back into the correct order?

* `Composed Message Processor` - How can you maintain the overall message flow when processing a message consisting of multiple elements, each of which may require different processing?

* `Scatter-Gather` - How do you maintain the overall message flow when a message needs to be sent to multiple recipients, each of which may send a reply?

* `Routing Slip` - How do we route a message consecutively through a series of processing steps when the sequence of steps is not known at design-time and may vary for each message?

* `Process Manager` - How do we route a message through multiple processing steps when the required steps may not be known at design-time and may not be sequential?

* `Message Broker` - How can you decouple the destination of a message from the sender and maintain central control over the flow of messages?

* `Throttler` - How can I throttle messages to ensure that a specific endpoint does not get overloaded, or we don’t exceed an agreed SLA with some external service?

* `Sampling` - How can I sample one message out of many in a given period to avoid downstream route does not get overloaded?

* `Delayer` - How can I delay the sending of a message?

* `Load Balancer` - How can I balance load across a number of endpoints?

* `Circuit Breaker` - How can I stop to call an external service if the service is broken?

* `Service Call` - How can I call a remote service in a distributed system where the service is looked up from a service registry of some sorts?

* `Saga` - How can I define a series of related actions in a Camel route that should be either completed successfully (all of them) or not-executed/compensated?

* `Multicast` - How can I route a message to a number of endpoints at the same time?

* `Loop` - How can I repeat processing a message in a loop?

### Message Transformation

* `Content Enricher` - How do we communicate with another system if the message originator does not have all the required data items available?

* `Content Filter` - How do you simplify dealing with a large message, when you are interested only in a few data items?

* `Claim Check` - How can we reduce the data volume of message sent across the system without sacrificing information content?

* `Normalizer` - How do you process messages that are semantically equivalent, but arrive in a different format?

* `Sort` - How can I sort the body of a message?

* `Script` - How do I execute a script which may not change the message?

* `Validate` - How can I validate a message?

### Messaging Endpoints

* `Messaging Mapper` - How do you move data between domain objects and the messaging infrastructure while keeping the two independent of each other?

* `Event Driven Consumer` - How can an application automatically consume messages as they become available?

* `Polling Consumer` - How can an application consume a message when the application is ready?

* `Competing Consumers` - How can a messaging client process multiple messages concurrently?

* `Message Dispatcher` - How can multiple consumers on a single channel coordinate their message processing?

* `Selective Consumer` - How can a message consumer select which messages it wishes to receive?

* `Durable Subscriber` - How can a subscriber avoid missing messages while it’s not listening for them?

* `Idempotent Consumer` - How can a message receiver deal with duplicate messages?

* `Transactional Client` - How can a client control its transactions with the messaging system?

* `Messaging Gateway` - How do you encapsulate access to the messaging system from the rest of the application?

* `Service Activator` - How can an application design a service to be invoked both via various messaging technologies and via non-messaging techniques?

### System Management

* `ControlBus` - How can we effectively administer a messaging system that is distributed across multiple platforms and a wide geographic area?

* `Detour` - How can you route a message through intermediate steps to perform validation, testing or debugging functions?

* `Wire Tap` - How do you inspect messages that travel on a point-to-point channel?

* `Message History` - How can we effectively analyze and debug the flow of messages in a loosely coupled system?

* `Log` - How can I log processing a message?

* `Step` - Steps groups together a set of EIPs into a composite logical unit for metrics and monitoring.

